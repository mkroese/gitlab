import { __, s__ } from '~/locale';

export const subscriptionDetailsHeaderText = s__('CloudLicense|Subscription details');
export const licensedToHeaderText = s__('CloudLicense|Licensed to');
export const manageSubscriptionButtonText = s__('CloudLicense|Manage');
export const syncSubscriptionButtonText = s__('CloudLicense|Sync Subscription details');
export const copySubscriptionIdButtonText = __('Copy');
export const detailsLabels = {
  address: __('Address'),
  id: s__('CloudLicense|ID'),
  company: __('Company'),
  email: __('Email'),
  lastSync: s__('CloudLicense|Last Sync'),
  name: __('Name'),
  plan: s__('CloudLicense|Plan'),
  startsAt: s__('CloudLicense|Started'),
  renews: s__('CloudLicense|Renews'),
};
